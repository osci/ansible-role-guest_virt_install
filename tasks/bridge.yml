---

- name: "Add/Remove Bridge to Guest VM"
  guest_bridge:
    name: "{{ vm_name }}"
    bridge: "{{ bridge }}"
    state: "{{ bridge_state }}"

- name: "Fetch VM facts"
  setup:
  delegate_to: "{{ vm_name }}"
  delegate_facts: True

- name: "Manage Bridge<->Iface Mapping in Guest VM"
  block:
    - name: "Install lxml"
      package:
        name: "{{ (ansible_python['version']['major'] == 3) | ternary('python3-lxml', 'python-lxml') }}"

    - name: "Fetch VM libvirt configuration"
      virt:
        command: get_xml
        name: "{{ vm_name }}"
      register: vm_xml

    - name: "Find Bridge"
      xml:
        xmlstring: "{{ vm_xml['get_xml'] }}"
        xpath: "/domain/devices/interface[@type='bridge']/source[@bridge='{{ bridge }}']/parent::*/mac"
        content: attribute
      register: iface_xml

    # missing on RedHat systems
    - name: "Create Directory for Systemd Network Configs"
      file:
        path: /etc/systemd/network/
        state: directory
        owner: root
        group: root
        mode: 0755
      delegate_to: "{{ vm_name }}"

    - name: "Ensure VLAN ifaces are not renamed"
      copy:
        src: systemd_vlan.link
        dest: /etc/systemd/network/09_vlan.link
        owner: root
        group: root
        mode: 0644
      delegate_to: "{{ vm_name }}"
      notify:
        - Regenerate initramfs images
        - Host needs reboot -- guest_virt_install

    - name: "Create ifaces mapping using systemd"
      template:
        src: systemd_iface.link
        dest: "/etc/systemd/network/10_{{ bridge }}.link"
        owner: root
        group: root
        mode: 0644
      delegate_to: "{{ vm_name }}"
      notify:
        - Regenerate initramfs images
        - Host needs reboot -- guest_virt_install

  when: hostvars[vm_name].ansible_service_mgr == 'systemd' and bridge_state == 'present' and bridge_iface is defined

- name: "Remove Bridge<->Iface Mapping in Guest VM"
  block:
    - name: "Remove ifaces mapping using systemd"
      file:
        path: "/etc/systemd/network/10_{{ bridge }}.link"
        state: absent
      delegate_to: "{{ vm_name }}"
      notify:
        - Regenerate initramfs images
        - Host needs reboot -- guest_virt_install

  when: hostvars[vm_name].ansible_service_mgr == 'systemd' and bridge_state == 'absent' and bridge_iface is defined

