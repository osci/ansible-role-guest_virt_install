# Ansible role for creating libvirt VMs

This role does the following:

- create storage for the VM using LVM
- generate an unattended installation configuration for the target distribution
- if the VM is missing:
  - run the VM with the CPU/RAM/network parameters and start the unattended installation
  - wait for the installation to reboot
  - add the VM SSH key to your local known_hosts file

One example of a playbook to do that:

```
- hosts: virt01.example.org
  tasks:
    - include_role:
        name: guest_virt_install
      vars:
        vm_name: guest.example.org
        system_disk_size: 8
        volgroup: volume01
        mem_size: 2048
        num_cpus: 2
        distribution: Fedora
        version: 22
        bridge: virbr0
        #bridge_iface: eth-lan
        network:
          proto: 'dhcp'
```

For now, this is a very basic wrapper around virt-install, but more options
are gonna be added.

## Disk layout

By default, the VM will be installed with a single disk used for system, and that can be controlled
with the 2 parameters `volgroup` and `system_disk_size`. You can decide to add a extra disks for data,
that will not be erased as part of the installation, thus permitting to easily reinstall the whole
system if needed.

The disks are created as a logic volume on the volume group `volgroup` on the server.

You can set the disk sizes, expressed in Gb, with `system_disk_size` and the list `data_disks_sizes`.

## CPU and Memory

Unless specified otherwise with `num_cpus`, the VM will have 1 CPU. It can be hotplugged to
2 \* num_cpus, see libvirt and virsh documentation for how to do it.

The memory must be expressed in Mb, so 1Gb would be 1024. There is no default value as of now. The
role do verify that we do allocate enough memory for installing distributions. Like the cpus, memory
can extended without reboot up to twice the amount of normal memory.

## Limitation

Beware, the system only make sure that the VM is installed and by default start it, but will
not make sure the VM is started if the admin decide to stop it manually or manage it using another
role/playbook. This is a difference with the service module.

## Supported distributions

Anaconda based distributions (Fedora and Centos) are supported and stable.
Debian and Ubuntu support is quite recent (october 2017) and while it was tested, it is much more
recent than anaconda-based ones, so bugs can happen.

Others OS (ie, various BSDs) would requires support in virt-install and a way to inject automated
answers to the installer. There is no straight forward way for most of them.

## Network configuration

In order to configure the network, you need to fill a structure for the `network` argument. There
is 2 types of setup, dhcp and static

### DHCP Network configuration

By default, the role will default to DHCP if no configuration is given.

Previous version of the role would take a `bootproto` entry in the `network` dictionary
but this is now automatically detected and not needed anymore.

### Static Network configuration

For static setup, a `network` structure must be given, with the
`ip` (ip and subnet information) and `gateway` being mandatory keys.

`ip` being the ipv4 address, with the netmask being required.
`gateway` would be the default route, again using ipv4.

Here is a example of the setup:

```
- hosts: virt01.example.org
  tasks:
    - include_role:
        name: guest_virt_install
      vars:
        vm_name: guest.example.org
        ...
        network:
          ip: 10.0.0.15/24
          gateway: 10.0.0.1
```

### DNS settings

In order to simplify parameters sharing, DNS can also be specified as a first level
variable instead of being part of the `network` dictionary. If left unspecified, default
from the role (currently, the Google DNS) would apply. It can be overriden with group_vars
or host_vars, or even on a per host basis.

### Add/Remove interfaces

You can add or remove interfaces in the VM and connect them to a bridge
of your liking on the host.

Using the `bridge` entrypoint and providing the name of the `bridge`
will add a new interface connected to this bridge. By default the
`present` parameter is True which mean creation of a new interface, but
can be set to `no` to remove the interface.

Example:

```
- hosts: virt01.example.org
  tasks:
    # add a new interface
    - include_role:
        name: guest_virt_install
        tasks_from: bridge
      vars:
        vm_name: guest.example.org
        bridge: virbr1
        #bridge_iface: eth-wan
    # remove an interface
    - include_role:
        name: guest_virt_install
        tasks_from: bridge
      vars:
        vm_name: guest.example.org
        bridge: virbr2
        bridge_state: absent
```

Calling this entrypoint on an already existing or absent interface is
idempotent.

This entrypoint obsoletes the `guest_add_bridge` role.

### Interface (re)naming

The installation interface in the VM will be called `eth0`. Then if you
add interfaces as explained in the previous chapter. Unfortunately the
name of these interfaces cannot be predicted and may change upon reboot.
Even the installation interface might switch with another.

To solve this problem we implemented a way to reliably name an
interface. This method requires systemd on the VM and is only enabled if
you specify the `bridge_iface` parameter, either in the initial
installation or when adding a new interface with the `bridge`
entrypoint. Previous examples have this parameter commented-out to show
where and how it can be used.

When this parameter is provided, this role, after the interface is added
by libvirt, will ask libvirt about the bridge information to fetch the
VM interface MAC. It will then create a systemd link file to force the
mapping and regenerate the initramfs. Another link file is create to
ensure VLAN interfaces, sharing the same MAC, are not matched.

If you use this feature at installation time, then you will lose
network after reboot. To avoid that the role makes some adaptations:

- on Debian, rename the interface in /etc/network/interfaces
- on RedHat, if present, rename /etc/sysconfig/network-scripts/ifcfg-eth0
  and rename interface name inside
- if Network Manager is active, clone the eth0 connection and update
  the cloned connection to match the new interface name
  (the old eth0 connection will stay around unused though)

To apply these changes and start deploying on the VM a reboot is
needed. `vm_reboot_ok` controls if the reboot is allowed in a handler.
If unset (the default) then reboot is only allowed at creation time.

## Retrieving VM distribution configuration and root password

The VM settings generated by this role can be found under `/etc/libvirt/kickstarts/<vm-name>`
or `/etc/libvirt/preseeds/<vm-name>`, respectively for Red Hat- or Debian-style systems.

This directory contains the following files:

- `ks.cfg`: the kickstart file for Anaconda on Red Hat systems
- `preseed.cfg`: the Debian installer configuration on Debian systems
- `password`: the root password
